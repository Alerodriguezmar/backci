package com.viajesweb.services;

import java.util.List;
import java.util.Optional;

import com.viajesweb.models.City;

public interface ICityService {

	Optional<City> get(int id);

	City getCityName(String cityName);

	List<City> cityList();

	void addCity(City city);

	void deleteCity(Integer id);
}
