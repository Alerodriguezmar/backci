package com.viajesweb.services;

import java.util.List;
import com.viajesweb.models.Travel;

public interface ITravelService {

	List<Travel> listTravel();

	void addTravel(String touristIdentification, String cityName, Travel travel, String travelDate);

	void addTravelOptional(Travel travel);

	List<Travel> getCity(int id);

	List<Travel> getTourist(String identification);

	void deleteTravel(Integer id);

	void sendMail(String from, String to, String subject, String body);
}
