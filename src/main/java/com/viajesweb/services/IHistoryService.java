package com.viajesweb.services;

import java.util.List;
import com.viajesweb.models.History;

public interface IHistoryService {

	 List<History> historyList();
	 
	 List<History> cityHistoryList(String cityName);
	 
	 List<History> touristHistoryList(String identification);
}
