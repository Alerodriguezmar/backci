package com.viajesweb.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.viajesweb.models.Tourist;
import com.viajesweb.respositories.TouristRepository;

@Service
public class TouristService implements ITouristService {

	@Autowired
	private TouristRepository touristRepository;
	
	/**
	 * Busca una turista por su identificador .
	 * 
	 * @param id indica el valor del indentificador a buscar en la tabla tourist.
	 * @return Un turista cuyo valor de id o identificador es igual a (id).
	 */
	@Override
	public Optional<Tourist> get(int id) {
		return touristRepository.findById(id);
	}

	/**
	 * Restorna todas los turistas existentes en la tabla tourist
	 * 
	 * @return Una lista de todos los turistas encontrados de lo cotrario retornara
	 *         una lista vacia.
	 */
	@Override
	public List<Tourist> touristList() {
		return (List<Tourist>) touristRepository.findAll();
	}

	/**
	 * Se encarga de insertar un nuevo turista en la tabla tourist. En caso de que se
	 * encuentre el turista entonces la información se sobreescribe.
	 * 
	 * @param Tourist tourist Corresponde a la información de un nuevo turista a insertar.
	 */
	@Override
	public void addTourist(Tourist tourist) {
		touristRepository.save(tourist);
	}
	
	/**
	 * Elimina un turista en la tabla tourist por su identificador.
	 * 
	 * @param id identificador de un turista a eliminar en la tabla tourist
	 */
	@Override
	public void deleteTourist(Integer id) {
		touristRepository.deleteById(id);
	}
}
