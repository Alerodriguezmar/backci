package com.viajesweb.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CityDTO {
	
	int idCity;
	String cityName;
	int amountPopulation;
	String touristicPlace;
	String reservedHotel;
}
