package com.viajesweb.dto;

import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TouristDTO {
	
	 int idTourist;
	 String touristName;
	 String touristLastname;
	 LocalDate birthDate;
	 String identification;
	 String typeId;
	 int travelFrequency;
	 Double travelBudget;
     boolean creditCard;
}
