package com.viajesweb.dto;

import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HistoryDTO {

	int idHistory;
    String cityName;
    String touristName;
    String touristIdentification;
    LocalDate admissionDate;
}
