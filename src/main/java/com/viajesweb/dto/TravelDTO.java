package com.viajesweb.dto;

import java.time.LocalDate;
import com.viajesweb.models.City;
import com.viajesweb.models.Tourist;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TravelDTO {

	 int idTravel;
	 LocalDate travelDate;
	 Tourist tourist;
	 City city;
}
