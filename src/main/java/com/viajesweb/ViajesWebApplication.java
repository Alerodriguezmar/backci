package com.viajesweb;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class ViajesWebApplication {

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

	@Value("${com.url}")
	private String url;

	@Value("${com.methods}")
	private String method;

	@Value("${com.mapping}")
	private String mapping;

	@Bean
	@CrossOrigin
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping(mapping).allowedOrigins(url).allowedMethods(method);
			}
		};
	}

	public static void main(String[] args) {
		SpringApplication.run(ViajesWebApplication.class, args);
	}
}
