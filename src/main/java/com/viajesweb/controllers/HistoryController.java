package com.viajesweb.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import com.viajesweb.models.History;

import com.viajesweb.services.IHistoryService;

@RestController
public class HistoryController {

	@Autowired
	private IHistoryService serviceHistory;

	/**
	 * Restorna todas las asignaciones a viajes registradas en la tabla travel junto
	 * a su hora de ingreso
	 * 
	 * @return Una lista de todas las asignaciones encontradas de lo cotrario
	 *         retornara una lista vacia.
	 */
	@GetMapping("/historial")
	public List<History> listHistory() {
		return serviceHistory.historyList();
	}

	/**
	 * Restorna todas las asignaciones de viajes registradas con el nombre de una
	 * ciudad en la tabla history junto a su hora de ingreso
	 * 
	 * @return Una lista de todas las asignaciones encontradas para una ciudad de lo
	 *         cotrario retornara una lista vacia.
	 */
	@GetMapping("/historial/ciudad/{nameCity}")
	public List<History> listCityHistory(@PathVariable(value = "name") String name) {
		return serviceHistory.cityHistoryList(name);
	}

	/**
	 * Restorna todas las asignaciones de viajes registradas con el numero de
	 * identificacion de un turista en la tabla history junto a su hora de ingreso
	 * 
	 * @return Una lista de todas las asignaciones encontradas para un turista de lo
	 *         cotrario retornara una lista vacia.
	 */
	@GetMapping("/historial/turista/{identification}")
	public List<History> listTouristHistory(@PathVariable(value = "identification") String identification) {
		return serviceHistory.touristHistoryList(identification);
	}
}
