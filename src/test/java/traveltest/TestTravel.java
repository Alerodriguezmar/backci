package traveltest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.viajesweb.ViajesWebApplication;

import net.minidev.json.JSONObject;
@RunWith(SpringRunner.class)
@AutoConfigureWebTestClient
@SpringBootTest(classes = ViajesWebApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TestTravel {

	WebTestClient testClient = WebTestClient.bindToServer().baseUrl("http://localhost:8080").build();

	@Autowired
	JdbcTemplate jdbc;
	
	@Test
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts = "DeleteTravelTableTest.sql")
	@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "TestInsertTravel.sql")
	void testGetTravelCity() throws JSONException {
		

		String countRow = " Select count(*) from travel";
		String consult = "Select * from travel where id_travel = 10";
		int countResult = jdbc.queryForObject(countRow, Integer.class);
		String expect = "[{ID_TRAVEL=10, "
				+ "TRAVEL_DATE=2021-08-15 00:00:00.0,"
				+ " ID_CITY=10, "
				+ "ID_TOURIST=10}]";
		
		assertEquals(1, countResult);
		testClient.get().uri("/viajesCiudad/10").accept(MediaType.APPLICATION_JSON).exchange().expectStatus().isOk();
		String result = jdbc.queryForList(consult).toString();
		assertEquals(expect, result);		
	}
	
	@Test
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts = "DeleteTravelTableTest.sql")
	@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "TestInsertTravel.sql")
	void testGetTravelTourist() throws JSONException {
		

		String countRow = " Select count(*) from travel";
		String consult = "Select * from travel where id_travel = 10";
		int countResult = jdbc.queryForObject(countRow, Integer.class);
		String expect = "[{ID_TRAVEL=10, "
				+ "TRAVEL_DATE=2021-08-15 00:00:00.0,"
				+ " ID_CITY=10, "
				+ "ID_TOURIST=10}]";
		
		assertEquals(1, countResult);
		testClient.get().uri("/viajesTurista/123").accept(MediaType.APPLICATION_JSON).exchange().expectStatus().isOk();
		String result = jdbc.queryForList(consult).toString();
		assertEquals(expect, result);		
	}
	
	
	@Test
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts = "DeleteTravelTableTest.sql")
	@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "TestInsertTravel.sql")
	void testDeleteTravel() {

		int deletedId = 10;
		String consult = "Select * from travel where id_travel = 10";
		String countRow = " Select count(*) from travel";

		int countResult = jdbc.queryForObject(countRow, Integer.class);
		assertEquals(1, countResult);
		testClient.delete().uri("/viajes/" + deletedId).accept(MediaType.APPLICATION_JSON).exchange().expectStatus()
				.isOk();
		int countResult2 = jdbc.queryForObject(countRow, Integer.class);
		assertEquals(0, countResult2);
		assertThat(jdbc.queryForList(consult).isEmpty()).isTrue();
	}
	
	@Test
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts = "DeleteTravelTableTest.sql")
	@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "TestInsertTravelObjects.sql")
	void testInsertTravel() {
		
		
		JSONObject objCity = new JSONObject();
		objCity.appendField("idCity", "10");
		objCity.appendField("cityName", "Bogota");
		objCity.appendField("amountPopulation", "987456");
		objCity.appendField("touristicPlace", "Desierto");
		objCity.appendField("reservedHotel", "Central");

		
		JSONObject objTourist = new JSONObject();
		objTourist.appendField("idTourist", "10");
		objTourist.appendField("touristName", "Diego");
		objTourist.appendField("touristLastname", "rodriguez");
		objTourist.appendField("birthDate", "2021-08-13");
		objTourist.appendField("identification", "123");
		objTourist.appendField("typeId", "cd");
		objTourist.appendField("travelFrequency", "5");
		objTourist.appendField("travelBudget", "5.0");
		objTourist.appendField("creditCard", "true");
		
		JSONObject objTravel = new JSONObject();
		objTravel.appendField("idTravel", "10");
		objTravel.appendField("tourist", objTourist);
		objTravel.appendField("city", objCity);
		objTravel.appendField("travelDate", "2021-08-13");
		
		//testClient.post().uri("/viajes").bodyValue(objTravel).accept(MediaType.APPLICATION_JSON).exchange().expectStatus()
		//.isOk();	
	}
	
}
