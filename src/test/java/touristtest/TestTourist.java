package touristtest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import com.viajesweb.ViajesWebApplication;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.ParseException;

@RunWith(SpringRunner.class)
@AutoConfigureWebTestClient
@SpringBootTest(classes = ViajesWebApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TestTourist {

	WebTestClient testClient = WebTestClient.bindToServer().baseUrl("http://localhost:8080").build();

	@Autowired
	JdbcTemplate jdbc;

	@Test
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts = "DeleteTouristTableTest.sql")
	@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "DeleteTouristTableTest.sql")
	@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "TestInsertTourist.sql")
	void testGetTourist() throws JSONException {

		String countRow = " Select count(*) from tourist";
		int countResult = jdbc.queryForObject(countRow, Integer.class);

		String expect = " {\"idTourist\": 1,\""
				+ "touristName\": \"tyesdt\","
				+ "\"touristLastname\": \"test\","
				+ " \"birthDate\": \"2021-08-12\","
				+ " \"identification\": \"123\","
				+ " \"typeId\": \"ID\","
				+ " \"travelFrequency\": 10,"
				+ " \"travelBudget\": 10.0,"
				+ "\"creditCard\":true}";

		assertEquals(1, countResult);
		testClient.get().uri("/turista/1").accept(MediaType.APPLICATION_JSON).exchange().expectStatus().isOk()
				.expectBody().json(expect);
	}

	@Test
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts = "DeleteTouristTableTest.sql")
	void testInsertTourist() throws ParseException {

		String consult = "Select * from tourist where id_tourist = 10";
		String countRow = " Select count(*) from tourist";

		String expect = "[{ID_TOURIST=10,"
				+ " BIRTH_DATE=2021-08-13 00:00:00.0"
				+ ", CREDIT_CARD=1, "
				+ "IDENTIFICATION=123,"
				+ " TOURIST_LASTNAME=rodriguez, "
				+ "TOURIST_NAME=Diego, "
				+ "TRAVEL_BUDGET=5, "
				+ "TRAVEL_FRECUENCY=5,"
				+ " TYPE_ID=cd}]";

		JSONObject obj = new JSONObject();
		obj.appendField("idTourist", "10");
		obj.appendField("touristName", "Diego");
		obj.appendField("touristLastname", "rodriguez");
		obj.appendField("birthDate", "2021-08-13");
		obj.appendField("identification", "123");
		obj.appendField("typeId", "cd");
		obj.appendField("travelFrequency", "5");
		obj.appendField("travelBudget", "5.0");
		obj.appendField("creditCard", "true");

		testClient.post().uri("/turista").bodyValue(obj).accept(MediaType.APPLICATION_JSON).exchange().expectStatus()
				.isOk();
		int countResult = jdbc.queryForObject(countRow, Integer.class);
		String result = jdbc.queryForList(consult).toString();
		assertEquals(1, countResult);
		assertThat(jdbc.queryForList(consult).isEmpty()).isFalse();
		assertEquals(expect, result);

	}

	@Test
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts = "DeleteTouristTableTest.sql")
	@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "TestDeleteTourist.sql")
	void testDeleteTourist() {

		int deletedId = 5;
		String consult = "Select * from tourist where id_tourist = 5";
		String countRow = " Select count(*) from tourist";

		int countResult = jdbc.queryForObject(countRow, Integer.class);
		assertEquals(1, countResult);
		testClient.delete().uri("/turista/" + deletedId).accept(MediaType.APPLICATION_JSON).exchange().expectStatus()
				.isOk();
		int countResult2 = jdbc.queryForObject(countRow, Integer.class);
		assertEquals(0, countResult2);
		assertThat(jdbc.queryForList(consult).isEmpty()).isTrue();
	}
	
	

	@Test
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts = "DeleteTouristTableTest.sql")
	@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "InsertUpdateTest.sql")
	void testUpdateTourist() {

		String consult = "Select * from tourist where id_tourist = 7";

		JSONObject obj = new JSONObject();
		obj.appendField("idTourist", "7");
		obj.appendField("touristName", "TEST53252");
		obj.appendField("touristLastname", "test");
		obj.appendField("birthDate", "2022-08-13");
		obj.appendField("identification", "123");
		obj.appendField("typeId", "CC");
		obj.appendField("travelFrequency", "5");
		obj.appendField("travelBudget", "5.0");
		obj.appendField("creditCard", "true");

		String expect = "[{ID_TOURIST=7,"
				+ " BIRTH_DATE=2022-08-13 00:00:00.0"
				+ ", CREDIT_CARD=1, "
				+ "IDENTIFICATION=123, "
				+ "TOURIST_LASTNAME=test, "
				+ "TOURIST_NAME=TEST53252,"
				+ " TRAVEL_BUDGET=5, "
				+ "TRAVEL_FRECUENCY=5, "
				+ "TYPE_ID=CC}]";

		assertThat(jdbc.queryForList(consult).isEmpty()).isFalse();
		testClient.put().uri("/turista").bodyValue(obj).accept(MediaType.APPLICATION_JSON).exchange().expectStatus()
				.isOk();
		String result = jdbc.queryForList(consult).toString();
		assertEquals(expect, result);
	}
}
