package citytest;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import com.viajesweb.ViajesWebApplication;

import net.minidev.json.JSONObject;

@RunWith(SpringRunner.class)
@AutoConfigureWebTestClient
@SpringBootTest(classes = ViajesWebApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TestCity {
	
	WebTestClient testClient = WebTestClient.bindToServer().baseUrl("http://localhost:8080").build();

	@Autowired
	JdbcTemplate jdbc;
	
	@Test
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts = "Deletec.sql")
	void testInsertCity() {
		
		String consult = "Select * from city where id_city = 10";
		String countRow = " Select count(*) from city";
		
		String expect = "[{ID_CITY=10, "
						+ "AMOUNT_POPULATION=987456,"
						+ " CITY_NAME=Bogota,"
						+ " RESERVED_HOTEL=Central,"
						+ " TOURISTIC_PLACE=Desierto}]";
		
		JSONObject obj = new JSONObject();
		obj.appendField("idCity", "10");
		obj.appendField("cityName", "Bogota");
		obj.appendField("amountPopulation", "987456");
		obj.appendField("touristicPlace", "Desierto");
		obj.appendField("reservedHotel", "Central");
		
		testClient.post()
				  .uri("/ciudad")
				  .bodyValue(obj)
				  .accept(MediaType.APPLICATION_JSON)
				  .exchange()
				  .expectStatus()
				  .isOk();
		
		int countResult = jdbc.queryForObject(countRow, Integer.class);
		
		String result = jdbc.queryForList(consult).toString();
		assertEquals(1, countResult);
		assertThat(jdbc.queryForList(consult).isEmpty()).isFalse();
		assertEquals(expect, result);
	}
	
	@Test
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts = "Deletec.sql")
	@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "insert.sql")
	void testGetCity() throws JSONException {

		String countRow = " Select count(*) from City";
		int countResult = jdbc.queryForObject(countRow, Integer.class);

		String expect = " {\"idCity\": 5,"
				+ "\"cityName\": \"Funza\","
				+ "\"amountPopulation\": 98165,"
				+ "\"touristicPlace\": \"Iglecia\","
				+ "\"reservedHotel\": \"Luxor Hotel\"}";

		assertEquals(1, countResult);
		testClient.get().uri("/ciudad/5").accept(MediaType.APPLICATION_JSON).exchange().expectStatus().isOk()
				.expectBody().json(expect);
	}
	
	@Test
	@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "insert.sql")
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts = "Deletec.sql")
	void testDeleteCity() {

		int deletedId = 5;
		String consult = "SELECT * "
				       + "FROM city "
				       + "WHERE id_city = 5";
		String countRow = " Select count(*) from city";

		int countResult = jdbc.queryForObject(countRow, Integer.class);
		assertEquals(1, countResult);
		testClient.delete().uri("/ciudad/" + deletedId).accept(MediaType.APPLICATION_JSON).exchange().expectStatus()
				.isOk();
		int countResult2 = jdbc.queryForObject(countRow, Integer.class);
		assertEquals(0, countResult2);
		assertThat(jdbc.queryForList(consult).isEmpty()).isTrue();
	}
	
	@Test
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts = "Deletec.sql")
	void testUpdateCity() {

		String consult = "Select * from city where id_city = 5";

		JSONObject obj = new JSONObject();
		obj.appendField("idCity", "5");
		obj.appendField("cityName", "Funza");
		obj.appendField("amountPopulation", "98165");
		obj.appendField("touristicPlace", "Desierto");
		obj.appendField("reservedHotel", "Central");

		String expect = "[{ID_CITY=5, "
				+ "AMOUNT_POPULATION=98165,"
				+ " CITY_NAME=Funza,"
				+ " RESERVED_HOTEL=Central,"
				+ " TOURISTIC_PLACE=Desierto}]";

		testClient.put().uri("/ciudad").bodyValue(obj).accept(MediaType.APPLICATION_JSON).exchange().expectStatus()
				.isOk();
		String result = jdbc.queryForList(consult).toString();
		assertEquals(expect, result);
	}
}
